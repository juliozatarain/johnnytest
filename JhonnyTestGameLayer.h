//
//  JhonnyTesGameLayer.h
//  pruebaJuegoJohnnyTest
//
//  Created by Julio Zatarain on 18/04/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d/cocos2d.h"
#import "CocosDenshion/CocosDenshion/SimpleAudioEngine.h"
@interface JhonnyTestGameLayer : CCLayerColor
{
    NSMutableArray * _monsters;
    NSMutableArray * _projectiles;
    CCLabelTTF *scoreLabel;
    
}
@property (atomic) NSInteger armaActual;
@property (atomic) NSInteger puntaje;
@property (atomic) NSInteger vidas;
@property (strong, atomic) NSMutableArray *arregloVidas;
@property (strong, atomic)     CCSprite *instrucciones;



+(CCScene *) scene;

@end
