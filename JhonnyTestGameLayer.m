//
//  HelloWorldLayer.m
//  pruebaJuegoJohnnyTest
//
//  Created by Julio Zatarain on 18/04/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//


// Import the interfaces
#import "JhonnyTestGameLayer.h"
// Needed to obtain the Navigation Controller
#import "johnnyMenuNivelesViewController.h"
#import "gameOverScene.h"
#pragma mark - HelloWorldLayer

// HelloWorldLayer implementation
@implementation JhonnyTestGameLayer
@synthesize armaActual, puntaje, vidas, arregloVidas;
// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CCSprite *background = [CCSprite spriteWithFile:@"pantallaNivel1.jpg"];
    background.position = ccp(winSize.width/2, winSize.height/2);

    CCSprite *johnny = [CCSprite spriteWithFile:@"personajeJT.png"];
    [johnny setOpacity:200.0f];
    johnny.position = ccp(winSize.width/2, 125.0f);
    
    
        
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	JhonnyTestGameLayer *layer = [JhonnyTestGameLayer node];
	
    
	// add layer as a child to scene
    [scene addChild:background];

    [scene addChild: layer];
    [scene addChild:johnny];
	// return the scene
	return scene;
}
- (void) addMonster {
    
    CCSprite * monster;
    NSInteger tipo = arc4random()%3;
    
    switch(tipo)
    {
        case 0 :
            monster  =  [CCSprite spriteWithFile:@"enemigoArana.png"];
        break;
        case 1 :
            monster  =  [CCSprite spriteWithFile:@"enemigoFantasmas.png"];
        break;
        case 2 :
            monster  =  [CCSprite spriteWithFile:@"enemigoMurcielago.png"];
        break;
    }
    monster.tag = tipo;
    bool hayAlgo=false;

    // Create the actions
    if(tipo == 2)
    {
        

         monster.anchorPoint = ccp(1.0, 1.0);// changing position actually – but measuring with sprite size
        // Determine where to spawn the monster along the Y axis
        CGSize winSize = [CCDirector sharedDirector].winSize;
        int minY = 100.0f;
        int maxY = 440.0f;
        int rangeY = maxY - minY;
        int actualY = (arc4random() % rangeY) + minY;
        int minX = monster.contentSize.width;
        int maxX = winSize.width - monster.contentSize.width/2;
        int rangeX = maxX - minX;
        int actualX = (arc4random() % rangeX) + minX;
        
        // Create the monster slightly off-screen along the right edge,
        // and along a random position along the Y axis as calculated above
        
        for (CCSprite *monster in _monsters)
        {
            if (CGRectIntersectsRect(monster.boundingBox, CGRectMake(actualX, actualY+60, 30.0f, 30.0f)))
            {
                hayAlgo=true;
                break;
            }
        }
        
        if(!hayAlgo)
        {
        [_monsters addObject:monster];
        // Determine speed of the monster
        int minDuration = 1.0;
        int maxDuration = 2.0;
        int rangeDuration = maxDuration - minDuration;
        int actualDuration = (arc4random() % rangeDuration) + minDuration;
        monster.position = ccp(actualX, actualY);
            
        // reproducimos el sonido
        [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_murcielago.mp3"];
        
        [self addChild:monster];
        CCRotateBy * rota = [CCRotateBy actionWithDuration:actualDuration angle:360];
        CCMoveTo * movArriba = [CCMoveTo actionWithDuration:1.0
                                                    position:ccp(monster.position.x, monster.position.y+60)];
            
         
            CCDelayTime *delay  = [CCDelayTime actionWithDuration: 3.0*(monster.tag+1)];
            
            CCCallBlockN * delayTerminado = [CCCallBlockN actionWithBlock:^(CCNode *node) {
                
                [self quitaVidaYPuntos:10*(monster.tag+1)];
                
            }];
    
        [monster runAction:[CCSequence actions:movArriba, rota, delay, delayTerminado, nil]];
        }
    }
    else if(tipo == 0)
    {
        
        // Determine where to spawn the monster along the Y axis
        CGSize winSize = [CCDirector sharedDirector].winSize;
        int minY = 100.0f;
        int maxY = 180.0f;
        int rangeY = maxY - minY;
        int actualY = (arc4random() % rangeY) + minY;
        int minX = monster.contentSize.width;
        int maxX = winSize.width - monster.contentSize.width -20.0f;
        int rangeX = maxX - minX;
        int actualX = (arc4random() % rangeX) + minX;
        
        // Create the monster slightly off-screen along the right edge,
        // and along a random position along the Y axis as calculated above
        
        for (CCSprite *monster in _monsters)
        {
            if (CGRectIntersectsRect(monster.boundingBox, CGRectMake(actualX+100, actualY, 30.0f, 30.0f)))
            {
                hayAlgo=true;
                break;
            }
        }
        
        if(!hayAlgo)
        {

        [_monsters addObject:monster];
        // Determine speed of the monster
        int minDuration = 1.0;
        int maxDuration = 2.0;
        int rangeDuration = maxDuration - minDuration;
        int actualDuration = (arc4random() % rangeDuration) + minDuration;
        monster.position = ccp(actualX, actualY);
            
            // reproducimos el sonido
            [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_arana.mp3"];

        [self addChild:monster];
        // Create the actions
        CCMoveTo * actionMove = [CCMoveTo actionWithDuration:actualDuration
                                                    position:ccp(actualX+100, actualY)];
        CCDelayTime *delay  = [CCDelayTime actionWithDuration: 2.0*(monster.tag+1)];
            
        CCCallBlockN * delayTerminado = [CCCallBlockN actionWithBlock:^(CCNode *node) {
            
            [self quitaVidaYPuntos:10*(monster.tag+1)];
            
        }];
        [monster runAction:[CCSequence actions:actionMove, delay,delayTerminado, nil]];
        }
    }
    else
    {
        // Determine where to spawn the monster along the Y axis
        CGSize winSize = [CCDirector sharedDirector].winSize;
        int minY = monster.contentSize.height;
        int maxY = 420.0f;
        int rangeY = maxY - minY;
        int actualY = (arc4random() % rangeY) + minY;
        int minX = monster.contentSize.width;
        int maxX = winSize.width - monster.contentSize.width/2;
        int rangeX = maxX - minX;
        int actualX = (arc4random() % rangeX) + minX;
        
        
        for (CCSprite *monster in _monsters)
        {
            if (CGRectIntersectsRect(monster.boundingBox, CGRectMake(actualX, actualY, 30.0f, 30.0f)))
            {
                hayAlgo=true;
                break;
            }
        }
        
        if(!hayAlgo)
        {
            [_monsters addObject:monster];

        monster.opacity = 0.0f;
            
            // reproducimos el sonido
            [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_fantasma.mp3"];
            
        [self addChild:monster];
        monster.position = ccp(actualX, actualY);
        // Create the actions
        CCFadeIn * actionMove = [CCFadeIn actionWithDuration:1.0];
            
        CCDelayTime *delay  = [CCDelayTime actionWithDuration: 1.5*(monster.tag+1)];
            
        CCCallBlockN * delayTerminado = [CCCallBlockN actionWithBlock:^(CCNode *node) {
                
                [self quitaVidaYPuntos:10*(monster.tag+1)];
                
            }];
            [monster runAction:[CCSequence actions:actionMove, delay,delayTerminado, nil]];
   
        }
    }
}
- (void)update:(ccTime)dt {
    
    
    
    }

-(void)quitaVidaYPuntos:(NSInteger)puntos
{
    [self quitaPuntos:puntos];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_vida_menos.mp3"];
    switch ([self vidas]) {
        case 1:
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
           
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0f scene:[gameOverScene scene]]];

            break;
        case 2:
            [[[self arregloVidas] objectAtIndex:1] removeFromParentAndCleanup:YES];
            [self setVidas:[self vidas]-1];
            break;
        case 3:
            [[[self arregloVidas] objectAtIndex:2] removeFromParentAndCleanup:YES];
            [self setVidas:[self vidas]-1];
            break;
        default:
            break;
    }
}
// on "init" you need to initialize your instance
-(id) init
{
    
    
    
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
    if ((self = [super initWithColor:ccc4(0,0,0,0)])) {
        
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:0.5];
        
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"cautious-path.mp3"];
        
        
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        
        [self setInstrucciones:[CCSprite spriteWithFile:@"pantallaInstrucciones.png"]];
        
        [[self instrucciones] setPosition:ccp(winSize.width/2, winSize.height/2)];
        
        [self addChild:[self instrucciones] z:3];
        _monsters = [[NSMutableArray alloc] init];
        [self setArregloVidas:[[NSMutableArray alloc]init]];
        [self setVidas:3];
        //Set the score to zero.
        [self setPuntaje:0];
        
        //Create and add the score label as a child.
        scoreLabel = [CCLabelTTF labelWithString:@"0" fontName:@"Marker Felt" fontSize:30];
        scoreLabel.position = ccp(950, 690); //Middle of the screen...
        [self addChild:scoreLabel z:1];
        
        for(int x=0; x<3; x++)
        {
            CCSprite *vida = [CCSprite spriteWithFile:@"heart.png"];
            vida.position = ccp(40.0f + (x*60.0f), 584.0f);
            [self addChild:vida];
            [[self arregloVidas] insertObject:vida atIndex:x];

        }
        CCMenuItem *botonArma1 = [CCMenuItemImage itemWithNormalImage:@"botonArma1.png" selectedImage:@"botonArma1.png" target:self selector:@selector(armaSeleccionada:)];
        botonArma1.tag=0;
        CCMenuItem *botonArma2 = [CCMenuItemImage itemWithNormalImage:@"botonArma2.png" selectedImage:@"botonArma2.png" target:self selector:@selector(armaSeleccionada:)];
        botonArma2.tag=1;
        CCMenuItem *botonArma3 = [CCMenuItemImage itemWithNormalImage:@"botonArma3.png" selectedImage:@"botonArma3.png" target:self selector:@selector(armaSeleccionada:)];
        botonArma3.tag=2;
        CCMenuItem *botonArma4 = [CCMenuItemImage itemWithNormalImage:@"botonArma4.png" selectedImage:@"botonArma4.png" target:self selector:@selector(armaSeleccionada:)];
        botonArma4.tag=3;
        CCMenuItem *botonArma5 = [CCMenuItemImage itemWithNormalImage:@"botonArma5.png" selectedImage:@"botonArma5.png" target:self selector:@selector(armaSeleccionada:)];
        botonArma5.tag=4;
        CCMenuItem *botonArma6 = [CCMenuItemImage itemWithNormalImage:@"botonArma6.png" selectedImage:@"botonArma6.png" target:self selector:@selector(armaSeleccionada:)];
        botonArma6.tag=5;
        CCMenu *radioMenu =
        [CCMenu menuWithItems:botonArma1, botonArma2, botonArma3, botonArma4, botonArma5, botonArma6, nil];
        radioMenu.position = ccp(700, 590);
        [radioMenu alignItemsHorizontally];
        [botonArma1 selected];
        [self addChild:radioMenu];
        
        
         CCMenuItem *botonSalir = [CCMenuItemImage itemWithNormalImage:@"botonRegresar.png" selectedImage:@"botonRegresar.png" target:self selector:@selector(salir:)];
        CCMenu *menuSalir =
        [CCMenu menuWithItems:botonSalir, nil];
        menuSalir.position = ccp(winSize.width-(botonSalir.contentSize.width), winSize.height -(botonSalir.contentSize.height/2));
        [menuSalir alignItemsHorizontally];
        [self addChild:menuSalir];
        [self schedule:@selector(espera:) interval:5.0];
        [self setIsTouchEnabled:YES];
        [self schedule:@selector(update:)];
	}
	return self;
}

-(void)agregarPuntos:(NSInteger)puntos
{
    [self setPuntaje:[self puntaje] + puntos];
    [scoreLabel setString:[NSString stringWithFormat:@"%d", [self puntaje]]];
}
-(void)quitaPuntos:(NSInteger)puntos
{
    
    if([self puntaje]>=puntos)
        [self setPuntaje:[self puntaje] - puntos];
    else
        [self setPuntaje:0];
    
    [scoreLabel setString:[NSString stringWithFormat:@"%d", [self puntaje]]];
}

-(IBAction)armaSeleccionada:(CCMenuItem*)sender
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_boton.mp3"];

    [self setArmaActual:sender.tag];
}
-(IBAction)salir:(CCMenuItem*)sender
{
    
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    [[CCDirector sharedDirector] stopAnimation];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_boton.mp3"];
    
    [[[CCDirector sharedDirector]  presentingViewController]dismissViewControllerAnimated:YES completion:nil];

}
- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    
    // Choose one of the touches to work with
    UITouch *touch = [touches anyObject];
    
    
    CGPoint location = [self convertTouchToNodeSpace:touch];
    
    switch ([self armaActual])
    {
        case 0:
            [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_arma_1.mp3"];
            break;
        case 1:
            [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_arma_2.mp3"];
            break;
        case 2:
            [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_arma_3.mp3"];
            break;
        default:
            [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_arma_1.mp3"];
            break;
            
    }
    
    
    NSInteger tipo=99;
    for (CCSprite *monster in _monsters) {
        if (CGRectIntersectsRect(monster.boundingBox, CGRectMake(location.x, location.y,1.0f, 1.0f)) && [self armaActual] == monster.tag)
        {
            [[SimpleAudioEngine sharedEngine] playEffect:@"monstruo_muere.mp3"];
            
            tipo=monster.tag;
            [_monsters removeObject:monster];
            [self removeChild:monster cleanup:YES];
            break;
        }
    }
    if(tipo<99)
    [self agregarPuntos:10*(tipo+1)];


}
-(void)gameLogic:(ccTime)dt {
    [self addMonster];

}
-(void)espera:(ccTime)dt {
    [self unschedule: @selector(espera:)];

    CCFadeIn * actionMove = [CCFadeOut actionWithDuration:1.0];
    
    CCCallBlockN * delayTerminado = [CCCallBlockN actionWithBlock:^(CCNode *node) {
        
        [[self instrucciones] removeFromParentAndCleanup:YES];
        
    }];
    
    [[self instrucciones] runAction:[CCSequence actions:actionMove, delayTerminado, nil]];

    
    [self schedule:@selector(gameLogic:) interval:1.5];
    
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    _monsters = nil;
}


@end
