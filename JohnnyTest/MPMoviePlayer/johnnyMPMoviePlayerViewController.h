//
//  johnnyMPMoviePlayerViewController.h
//  JohnnyTest
//
//  Created by Julio Zatarain on 12/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

@interface johnnyMPMoviePlayerViewController : MPMoviePlayerViewController
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) NSURL *url;

-(void)destruyePlayer;
-(void) reproduceVideoEnLaVista:(UIView *)pagina conRect:(CGRect)rect;
-(void) moviePlayBackDidFinish:(NSNotification *)notification;
-(void) inicializaVideoConNombre:(NSString *)nombre extension:(NSString*)extension;
@end
