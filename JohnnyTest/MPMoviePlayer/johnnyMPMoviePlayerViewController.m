//
//  johnnyMPMoviePlayerViewController.m
//  JohnnyTest
//
//  Created by Julio Zatarain on 12/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import "johnnyMPMoviePlayerViewController.h"


@interface johnnyMPMoviePlayerViewController ()
@end

@implementation johnnyMPMoviePlayerViewController
@synthesize moviePlayer, url;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{ 
    [super didReceiveMemoryWarning];
}
//este metodo establece el video del movieplayercontroller
-(void) inicializaVideoConNombre:(NSString *)nombre extension:(NSString*)extension
{
    [self setUrl:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:nombre ofType:extension]]];
    [self setMoviePlayer:[[MPMoviePlayerController alloc] initWithContentURL:[self url]]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:[self moviePlayer]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didExitFullscreen:) name:MPMoviePlayerDidExitFullscreenNotification object:nil];

    
    [[self moviePlayer] setControlStyle:MPMovieControlStyleDefault];
    [[self moviePlayer] setShouldAutoplay:YES];
    [[self moviePlayer] prepareToPlay];
    
}
-(void)destruyePlayer
{
    [self setMoviePlayer:nil];
    [self setUrl:nil];
    
}
-(void) reproduceVideoEnLaVista:(UIView *)pagina conRect:(CGRect)rect;
{
    [[[self moviePlayer] view] setFrame:rect];
    [pagina addSubview:[[self moviePlayer] view]];
    [[self moviePlayer] setFullscreen:YES animated:YES];
    
}
-(void) moviePlayBackDidFinish:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:[self moviePlayer]];
    [[self moviePlayer] stop];
    [[self moviePlayer] setFullscreen:NO animated:YES];
    [[[self moviePlayer] view] removeFromSuperview];
    [self destruyePlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}
- (void)didExitFullscreen:(NSNotification*)notification {
    [[self moviePlayer] stop];
    [[[self moviePlayer] view] removeFromSuperview];
    [self destruyePlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
