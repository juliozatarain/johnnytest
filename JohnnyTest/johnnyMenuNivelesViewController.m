//
//  johnnyMenuNivelesViewController.m
//  JohnnyTest
//
//  Created by Julio Zatarain on 19/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import "johnnyMenuNivelesViewController.h"
#import "JhonnyTestGameLayer.h"
@interface johnnyMenuNivelesViewController ()

@end

@implementation johnnyMenuNivelesViewController
@synthesize window=window_, navController=navController_, director=director_, vista=vista_;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
	[super viewDidLoad];
  director_  =  [CCDirector sharedDirector];
    

    }
-(IBAction)regresaAMenuInicial:(id)sender{
    
   director_=nil;
    
    [[self navigationController] popViewControllerAnimated:YES];
}

-(IBAction)jugar:(id)sender
{
    
    [director_ pushScene: [JhonnyTestGameLayer scene]];
    [self presentViewController:director_ animated:YES completion: nil];
    

}
@end
