//
//  johnnyMenuInicialViewController.h
//  JohnnyTest
//
//  Created by Julio Zatarain on 17/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "../cocos2d/cocos2d.h"


@interface johnnyMenuInicialViewController : UIViewController <CCDirectorDelegate,AVAudioPlayerDelegate>
@property (strong, readonly) UINavigationController *navController;
@property (strong, readonly) CCDirector *director;
@property (strong, atomic) CCGLView *vista;
@end
