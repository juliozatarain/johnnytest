//
//  johnnyMenuReawardsViewController.m
//  JohnnyTest
//
//  Created by Julio Zatarain on 02/05/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import "johnnyMenuReawardsViewController.h"

@interface johnnyMenuReawardsViewController ()

@end

@implementation johnnyMenuReawardsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)regresar:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

@end
