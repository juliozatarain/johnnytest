//
//  johnnyJSONReader.h
//  JohnnyTest
//
//  Created by Julio Zatarain on 14/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface johnnyJSONReader : NSObject
@property (strong, nonatomic) NSData *data;


-(NSDictionary *)diccionarioVideos:(int)indice;
-(NSDictionary *)diccionarioAudios:(int)indice;
-(NSDictionary *)diccionarioDeTodasLasPaginas;

@end
