//
//  johnnyJSONReader.m
//  JohnnyTest
//
//  Created by Julio Zatarain on 14/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import "johnnyJSONReader.h"

@implementation johnnyJSONReader
@synthesize data;

- (id)init
{
    self = [super init];
    if (self) {
        
        [self setData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"paginas" ofType:@"json"]]];
    }
    return self;
}
-(NSDictionary *)diccionarioDeTodasLasPaginas
{
    NSError *error = nil;
    NSDictionary *jsonObjects = [NSJSONSerialization JSONObjectWithData:[self data] options:kNilOptions error:&error];
    return jsonObjects;
}
-(NSDictionary *)diccionarioAudios:(int)indice
{
    
    NSError *error = nil;    
    NSDictionary *jsonObjects = [NSJSONSerialization JSONObjectWithData:[self data] options:kNilOptions error:&error];
    return [jsonObjects objectForKey:@"audios"];

}

- (NSDictionary *)diccionarioVideos:(int)indice
{
    NSError *error = nil;
    NSDictionary *jsonObjects = [NSJSONSerialization JSONObjectWithData:[self data] options:kNilOptions error:&error];
    return [jsonObjects objectForKey:@"videos"];
}
@end
