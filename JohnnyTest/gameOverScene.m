//
//  HelloWorldLayer.m
//  pruebaJuegoJohnnyTest
//
//  Created by Julio Zatarain on 18/04/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//


// Import the interfaces
#import "gameOverScene.h"
// Needed to obtain the Navigation Controller
#import "johnnyMenuNivelesViewController.h"
#import "JhonnyTestGameLayer.h"

#pragma mark - gameOverScene

// HelloWorldLayer implementation
@implementation gameOverScene
// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CCSprite *background = [CCSprite spriteWithFile:@"pantallaGameOver.jpg"];
    background.position = ccp(winSize.width/2, winSize.height/2);
     
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	gameOverScene *layer = [gameOverScene node];
	
    
	// add layer as a child to scene
    [scene addChild:background];
    
    [scene addChild: layer];
	// return the scene
	return scene;
}
// on "init" you need to initialize your instance
-(id) init
{
    
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
    if ((self = [super initWithColor:ccc4(0,0,0,0)])) {
        CCMenuItem *botonReplay = [CCMenuItemImage itemWithNormalImage:@"boton_replay-ipad.png" selectedImage:@"boton_replay.png" target:self selector:@selector(jugarDeNuevo:)];
        
        
        
        CCMenu *menuReplay =
        [CCMenu menuWithItems:botonReplay, nil];
        menuReplay.position = ccp(485.0f, 305.f);
        [menuReplay alignItemsHorizontally];
        [self addChild:menuReplay];
        
        CCMenuItem *botonMenuNiveles= [CCMenuItemImage itemWithNormalImage:@"boton_levels.png" selectedImage:@"boton_levels.png" target:self selector:@selector(menuNiveles:)];
        
        
        
        CCMenu *menuNiveles =
        [CCMenu menuWithItems:botonMenuNiveles, nil];
        menuNiveles.position = ccp(745.0f, 310.f);
        [menuNiveles alignItemsHorizontally];
        [self addChild:menuNiveles];
        
        [self setIsTouchEnabled:YES];
	}
	return self;
}
// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
}
-(IBAction)jugarDeNuevo:(CCMenuItem*)sender
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_boton.mp3"];
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0f scene:[JhonnyTestGameLayer scene]]];

}
-(IBAction)menuNiveles:(CCMenuItem*)sender
{
    [[CCDirector sharedDirector] stopAnimation];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"sonido_boton.mp3"];

  [[[CCDirector sharedDirector]  presentingViewController]dismissViewControllerAnimated:YES completion:nil];
    
}

@end
