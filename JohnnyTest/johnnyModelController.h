//
//  johnnyModelController.h
//  JohnnyTest
//
//  Created by Julio Zatarain on 10/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "json/johnnyJSONReader.h"
#import "johnnyPageViewController.h"

@class johnnyDataViewController;

@interface johnnyModelController : NSObject <UIPageViewControllerDataSource>
@property (strong, atomic) johnnyJSONReader *lectorJson;
@property (strong, nonatomic) NSDictionary *diccionarioPaginas;

- (johnnyDataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(johnnyDataViewController *)viewController;
-(NSInteger)numDeControllers;
@end
