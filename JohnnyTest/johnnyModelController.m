//
//  johnnyModelController.m
//  JohnnyTest
//
//  Created by Julio Zatarain on 10/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import "johnnyModelController.h"

#import "johnnyDataViewController.h"


/*
 A controller object that manages a simple model -- a collection of month names.
 
 The controller serves as the data source for the page view controller; it therefore implements pageViewController:viewControllerBeforeViewController: and pageViewController:viewControllerAfterViewController:.
 It also implements a custom method, viewControllerAtIndex: which is useful in the implementation of the data source methods, and in the initial configuration of the application.
 
 There is no need to actually create view controllers for each page in advance -- indeed doing so incurs unnecessary overhead. Given the data model, these methods create, configure, and return a new view controller on demand.
 */

@interface johnnyModelController()
@end

@implementation johnnyModelController
@synthesize lectorJson, diccionarioPaginas;
- (id)init
{
    self = [super init];
    if (self) {      
        [self setLectorJson:[[johnnyJSONReader alloc]init]];
        [self setDiccionarioPaginas:[lectorJson diccionarioDeTodasLasPaginas]];
    }
    return self;
}

- (johnnyDataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard
{   
    // Return the data view controller for the given index.
    if (([self.diccionarioPaginas count] == 0) || (index >= [self.diccionarioPaginas count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    johnnyDataViewController *dataViewController = [storyboard instantiateViewControllerWithIdentifier:@"johnnyDataViewController"];
    
    dataViewController.numeroDePagina =index;
    dataViewController.datosDePagina  = [diccionarioPaginas objectForKey:[NSString stringWithFormat:@"%d", index]];
    return dataViewController;
}
-(NSDictionary *)dameDatosDePagina:(int)indice
{
    return [diccionarioPaginas objectForKey:[NSString stringWithFormat:@"%d", indice]];

}

- (NSUInteger)indexOfViewController:(johnnyDataViewController *)viewController
{   
     // Return the index of the given data view controller.
     // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    return viewController.numeroDePagina;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(johnnyDataViewController *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(johnnyDataViewController *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++; 
    if (index == [self.diccionarioPaginas count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
} 
-(NSInteger)numDeControllers
{
    return [self.diccionarioPaginas count];
}

@end
