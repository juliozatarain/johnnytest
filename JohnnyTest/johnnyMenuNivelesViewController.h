//
//  johnnyMenuNivelesViewController.h
//  JohnnyTest
//
//  Created by Julio Zatarain on 19/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../cocos2d/cocos2d.h"


@interface johnnyMenuNivelesViewController : UIViewController<CCDirectorDelegate>
{
	
    
}

@property (nonatomic, strong) UIWindow *window;
@property (strong, readonly) UINavigationController *navController;
@property (strong, readonly) CCDirector *director;
@property (strong, atomic)CCGLView *vista;


-(IBAction)regresaAMenuInicial:(id)sender;
@end
