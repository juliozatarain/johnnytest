//
//  main.m
//  JohnnyTest
//
//  Created by Julio Zatarain on 10/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "johnnyAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([johnnyAppDelegate class]));
    }
}
