//
//  johnnyPageViewController.m
//  Johnny_Trick_or_Treat
//
//  Created by Julio Zatarain on 08/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//


#import "johnnyPageViewController.h"

@interface johnnyPageViewController ()
{
    AVAudioPlayer *audioplayer;
    johnnyMPMoviePlayerViewController *controladorVideo;
    AVAudioPlayer *pageTurnPlayer;

}
@property (readonly, strong, nonatomic) johnnyModelController *modelController;

@end

@implementation johnnyPageViewController

@synthesize modelController = _modelController;
@synthesize botonRegresar = _botonRegresar;
@synthesize index = _index;

-(IBAction)play:(id)sender
{
    
    
    if([audioplayer isPlaying])
       [audioplayer stop];
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath],  [[[[[[self modelController] viewControllerAtIndex:_index storyboard:self.storyboard] datosDePagina] objectForKey:@"audios"] objectForKey:[NSString stringWithFormat:@"%d", [sender tag]]] objectForKey:@"nombre"]]];
        NSError *error;
        audioplayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    audioplayer.numberOfLoops = 0;;
        audioplayer.delegate=self;
        [audioplayer prepareToPlay];
      [audioplayer play];
    
    
}
- (void)videoConRect:(CGRect)rect
{
    [controladorVideo inicializaVideoConNombre:@"Jhonny_Test_Video_01" extension:@"mov"];
    [controladorVideo reproduceVideoEnLaVista:self.pageViewController.view conRect:rect];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/page-flip-2.mp3", [[NSBundle mainBundle] resourcePath]]];
    NSError *error;
    pageTurnPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    pageTurnPlayer.numberOfLoops = -1;;
    pageTurnPlayer.delegate=self;
    pageTurnPlayer.numberOfLoops= 0;
    [pageTurnPlayer prepareToPlay];

    // se establece que al cargar la vista no se estan realizando transiciones/animaciones de página
    //inicializamos el indice en 0;
    [self setIndex:0];
    controladorVideo = [[johnnyMPMoviePlayerViewController alloc] init];
    
    // Configure the page view controller and add it as a child view controller.
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageViewController.delegate = self;
    
    johnnyDataViewController *startingViewController = [self.modelController viewControllerAtIndex:0 storyboard:self.storyboard];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:NULL];
    
    self.pageViewController.dataSource = self.modelController;
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    
    // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
    CGRect pageViewRect = self.view.bounds;
        pageViewRect = CGRectInset(pageViewRect, 50.0, 50.0);    
    self.pageViewController.view.frame = pageViewRect;
    
    [self.pageViewController didMoveToParentViewController:self];
     
    self.view.gestureRecognizers = self.pageViewController.gestureRecognizers;
    
    UIGestureRecognizer* tapRecognizer = nil;
    for (UIGestureRecognizer* recognizer in self.pageViewController.gestureRecognizers) {
        if ( [recognizer isKindOfClass:[UITapGestureRecognizer class]] ) {
            tapRecognizer = recognizer;
            break;
        }
    }
    
    if ( tapRecognizer ) {
        [self.view removeGestureRecognizer:tapRecognizer];
        [self.pageViewController.view removeGestureRecognizer:tapRecognizer];
    }
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (johnnyModelController *)modelController
{
    // Return the model controller object, creating it if necessary.
    // In more complex implementations, the model controller may be passed to the view controller.
    if (!_modelController) {
        _modelController = [[johnnyModelController alloc] init];
    }
    return _modelController;
}

#pragma mark - UIPageViewController delegate methods

// este método se ejecuta antes de empezar la transición a otro view(pagina)
- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
    [pageTurnPlayer play];

      // deshabilitamos la interaccion para no permitir cambios de pagina durante transiciones
    [self.view setUserInteractionEnabled:NO];
    // bloqueamos el boton de regresar mientras esté una transición en progreso
    _botonRegresar.enabled=NO;
}
// este método se ejecuta una vez terminada la transición a otro view(pagina)
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
 {
     // desbloqueamos el boton de regresar cuando haya terminado la transición
     if(completed || finished)
     {
         _botonRegresar.enabled=YES;
         [self updatePageIndex];
         // habilitamos de nuevo la interacción con la vista
         [self.view setUserInteractionEnabled:YES];

     }

 }
- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    // solo permitimos  que se despliegue una pagina a la vez sin importar la orientación
    UIViewController *currentViewController = self.pageViewController.viewControllers[0];
    NSArray *viewControllers = @[currentViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:NULL];
    
    self.pageViewController.doubleSided = NO;
    return UIPageViewControllerSpineLocationMin;
    
}
-(void) updatePageIndex
{
    johnnyDataViewController *theCurrentViewController = [self.pageViewController.viewControllers objectAtIndex:0];
    
    [self setIndex:[self.modelController indexOfViewController:theCurrentViewController]];
  
}
-(IBAction)regresarAPantallaInicial:(id)sender
{
    [UIView animateWithDuration:0.8
                     animations:^{
                         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                         [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.navigationController.view cache:NO];
                     }];
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction) siguientePagina:(id)sender
{
    
    if([self index] < ([self.modelController numDeControllers]-1))
    {
        [pageTurnPlayer play];
       [self.view setUserInteractionEnabled:NO];
        johnnyDataViewController *destino = [self.modelController viewControllerAtIndex:[self index]+1 storyboard:self.storyboard];
        
        
        NSArray *viewControllers = nil;
        
        viewControllers = [NSArray arrayWithObjects:destino, nil];
            
        johnnyPageViewController *contexto = self;
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished){
            if(finished)[contexto updatePageIndex];
            
            [contexto.view setUserInteractionEnabled:YES];

        }];
    
    }

   
}
-(IBAction) paginaAnterior:(id)sender
{
    
    if([self index] > 0)
    {
        [pageTurnPlayer play];

        [self.view setUserInteractionEnabled:NO];
        johnnyDataViewController *destino = [self.modelController viewControllerAtIndex:[self index]-1 storyboard:self.storyboard];
        
        
        NSArray *viewControllers = nil;
        
        viewControllers = [NSArray arrayWithObjects:destino, nil];
        
        johnnyPageViewController *contexto = self;
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:^(BOOL finished){
            if(finished)[contexto updatePageIndex];
            [contexto.view setUserInteractionEnabled:YES];
        }];
        
    }
    
    
}

@end
