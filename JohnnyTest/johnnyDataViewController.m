//
//  johnnyDataViewController.m
//  JohnnyTest
//
//  Created by Julio Zatarain on 10/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import "johnnyDataViewController.h"
#import "johnnyPageViewController.h"

@interface johnnyDataViewController ()
{

}

@end

@implementation johnnyDataViewController
@synthesize datosDePagina,numeroDePagina;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    //crear los botones de audio y video
    [super viewWillAppear:animated];
    
    UIImageView *fondo = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 924.0f, 668.0f)];
    
    fondo.image = [UIImage imageNamed:[NSString stringWithFormat:@"fondoPagina%d.jpg", [self numeroDePagina]]];
    
    [self.view addSubview:fondo];

    int y = 0;
    for (NSString* key in [datosDePagina objectForKey:@"audios"])
    {
        NSDictionary *value = [[datosDePagina objectForKey:@"audios"] objectForKey:key];
        [self creaboton:0 posicionX: [[value objectForKey:@"x"] floatValue] posicionY: [[value objectForKey:@"y"] floatValue] indice:[[value objectForKey:@"tag"]intValue]];
    }
    y=0;
    for (NSString* key in [datosDePagina objectForKey:@"videos"])
    {
        NSDictionary *value = [[datosDePagina objectForKey:@"videos"] objectForKey:key];
        [self creaboton:1 posicionX: [[value objectForKey:@"x"] floatValue] posicionY: [[value objectForKey:@"y"] floatValue] indice:y];
        y++;
    }
}
-(void)creaboton:(int)tipo posicionX:(float)x posicionY:(float)y indice:(int)indice
{
    
    if(tipo==0)
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:[(UIPageViewController*)[self parentViewController] delegate]
                   action:@selector(play:)
         forControlEvents:UIControlEventTouchDown];
        [button setImage:[UIImage imageNamed:@"botonMusica.png"] forState:UIControlStateNormal];        button.frame = CGRectMake(x, y, 50.0f, 50.0f);
        button.tag = indice;
        [self.view addSubview:button];
    }
    else{
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self
                   action:@selector(reproduceVideo:)
         forControlEvents:UIControlEventTouchDown];
        [button setImage:[UIImage imageNamed:@"botonReproducir.png"] forState:UIControlStateNormal];
        button.frame = CGRectMake(x, y, 158.0f, 158.0f);
        [self.view addSubview:button];
        button.tag=indice;
        
    }
}
-(IBAction)reproduceVideo:(id)sender 
{
    NSDictionary *video= [[datosDePagina objectForKey:@"videos"] objectForKey:[NSString stringWithFormat:@"video%d", [sender tag]]];
    
   
    
    [(johnnyPageViewController*)[(UIPageViewController*)[self parentViewController] delegate] videoConRect: CGRectMake([[video objectForKey:@"xVista"] floatValue], [[video objectForKey:@"yVista"] floatValue], [[video objectForKey:@"anchoVista"] floatValue], [[video objectForKey:@"altoVista"] floatValue])];
}


@end
