//
//  johnnyDataViewController.h
//  JohnnyTest
//
//  Created by Julio Zatarain on 10/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface johnnyDataViewController : UIViewController <AVAudioPlayerDelegate>

@property (strong, nonatomic) NSDictionary *datosDePagina;
@property  int numeroDePagina;



@end
