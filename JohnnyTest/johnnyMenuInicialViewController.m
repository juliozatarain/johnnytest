//
//  johnnyMenuInicialViewController.m
//  JohnnyTest
//
//  Created by Julio Zatarain on 17/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import "johnnyMenuInicialViewController.h"

@interface johnnyMenuInicialViewController ()
{
    AVAudioPlayer *audioplayer;

}

@end

@implementation johnnyMenuInicialViewController
@synthesize vista=vista_, director=director_;
- (void)viewDidLoad
{
    [super viewDidLoad];
    

   
    // Create an CCGLView with a RGB565 color buffer, and a depth buffer of 0-bits
	vista_ = [CCGLView viewWithFrame:CGRectMake(0.0f, 0.0f, 1024.0f, 768.0f)
                         pixelFormat:kEAGLColorFormatRGB565	//kEAGLColorFormatRGBA8
                         depthFormat:0	//GL_DEPTH_COMPONENT24_OES
                  preserveBackbuffer:NO
                          sharegroup:nil
                       multiSampling:NO
                     numberOfSamples:0];
    
	director_  =  [CCDirector sharedDirector];
    
    
    
	// set FPS at 60
	[director_ setAnimationInterval:1.0/60];
    
	// attach the openglView to the director
	[director_ setView:vista_];
    
	// for rotation and other messages
	[director_ setDelegate:self];
    
	// 2D projection
	[director_ setProjection:kCCDirectorProjection2D];
    //	[director setProjection:kCCDirectorProjection3D];
    
	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
	if( ! [director_ enableRetinaDisplay:YES] )
		CCLOG(@"Retina Display Not supported");
    
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
    
	// If the 1st suffix is not found and if fallback is enabled then fallback suffixes are going to searched. If none is found, it will try with the name without suffix.
	// On iPad HD  : "-ipadhd", "-ipad",  "-hd"
	// On iPad     : "-ipad", "-hd"
	// On iPhone HD: "-hd"
	CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
	[sharedFileUtils setEnableFallbackSuffixes:NO];				// Default: NO. No fallback suffixes are going to be used
	[sharedFileUtils setiPhoneRetinaDisplaySuffix:@"-hd"];		// Default on iPhone RetinaDisplay is "-hd"
	[sharedFileUtils setiPadSuffix:@"-ipad"];					// Default on iPad is "ipad"
	[sharedFileUtils setiPadRetinaDisplaySuffix:@"-ipadhd"];	// Default on iPad RetinaDisplay is "-ipadhd"
    
	// Assume that PVR images have premultiplied alpha
	[CCTexture2D PVRImagesHavePremultipliedAlpha:YES];
    
    

}
-(void)viewWillDisappear:(BOOL)animated
{
    [audioplayer stop];
}
-(void)viewWillAppear:(BOOL)animated
{
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Jhonny_Test_Musica_Entrada.aiff", [[NSBundle mainBundle] resourcePath]]];
    NSError *error;
    audioplayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    audioplayer.numberOfLoops = -1;;
    audioplayer.delegate=self;
    [audioplayer prepareToPlay];
    [audioplayer play];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
