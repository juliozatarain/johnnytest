//
//  johnnyPageViewController.h
//  JohnnyTest
//
//  Created by Julio Zatarain on 10/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "johnnyModelController.h"
#import "johnnyDataViewController.h"
#import "MPMoviePlayer/johnnyMPMoviePlayerViewController.h"
@interface johnnyPageViewController : UIViewController <UIPageViewControllerDelegate, AVAudioPlayerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (atomic) NSInteger index;

@property (strong, nonatomic) IBOutlet  UIButton *botonRegresar;
- (void)videoConRect:(CGRect)rect;
@end
