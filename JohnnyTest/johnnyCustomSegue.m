//
//  johnnyCustomSegue.m
//  JohnnyTest
//
//  Created by Julio Zatarain on 17/04/13.
//  Copyright (c) 2013 DMS. All rights reserved.
//

#import "johnnyCustomSegue.h"

@implementation johnnyCustomSegue

- (void) perform {
    
    UIViewController *src = (UIViewController *) self.sourceViewController;
    UIViewController *dst = (UIViewController *) self.destinationViewController;
    [UIView transitionWithView:src.navigationController.view duration:0.8
                       options:UIViewAnimationOptionTransitionCurlUp
                    animations:^{
                        [src.navigationController pushViewController:dst animated:NO];
                    }
                    completion:NULL];
   
}
@end
